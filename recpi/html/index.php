<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <style type="text/css">
            #playlist,audio{background:#666;width:80%;max-width:400px;padding:20px;}
            .active a{color:#5DB0E6;text-decoration:none;}
            li a{color:#eeeedd;background:#333;padding:5px;display:block;}
            li a:hover{text-decoration:none;}
        </style>

        <script
	        type = "text/javascript"
	        src = "./jquery-1.7.js"
        ></script>
        
        <script type="text/javascript">


            $(window).load(function(){
      
            var audio;
            var playlist;
            var tracks;
            var current;

            init();
    
            function init(){
       	        current = 0;
            	audio = $('#audio');
            	playlist = $('#playlist');
    	        tracks = playlist.find('li a');
    	        len = tracks.length - 1;
    	        audio[0].volume = .90;
    	        playlist.find('a').click(function(e){
                        e.preventDefault();
                        link = $(this);
                        current = link.parent().index();
                        run(link, audio[0]);
    	            });
    	        audio[0].addEventListener('ended',function(e){
                        current++;
                        if(current > len){
                            current = 0;
                            link = playlist.find('a')[0];
                        }
	                    else{
                            link = playlist.find('a')[current];    
                        }
                        run($(link),audio[0]);
    	            });
                }
     
            function run(link, player){
                player.src = link.attr('href');
                par = link.parent();
                par.addClass('active').siblings().removeClass('active');
                audio[0].load();
                audio[0].play();
                }

            });

        </script>
    </head>
    
    <body>
        <center>
            <div > <img src="./wallpaper.jpg" style="width:90%"> </div>
            <br>	
            <font size="8px"> CiGa Barudak Radio </font> 
            <br><br>	
            <?php
    
                // Opens directory
                $myDirectory=opendir("/var/www/html/audios/");

                // Gets each entry
                while($entryName=readdir($myDirectory)) {
                    $dirArray[]=$entryName;
                }

                // Finds extensions of files
                function findexts($filename) {
                    for ($i=0;$filename[$i]!=NULL;$i++){}
	                $exts = $filename[$i-1];
                    return $exts;
                }

                // Closes directory
                closedir($myDirectory);

                // Counts elements in array
                $indexCount=count($dirArray);

                // Sorts files
                //sort($dirArray);
                rsort($dirArray);//sorting in descending order
	
                $flag=0;

                // Loops through the array of files
                for($index=0; $index < $indexCount; $index++) {

                    // Gets File Names
                    $name=$dirArray[$index];
                    $namehref=$dirArray[$index];	
                    $ext = findexts($name);
	
                    if ($ext=='3' || $ext=='g'){
	
	                    if ($flag==0){
	                        // Print 'em
  	                        
                            
                                print("
	                            <audio autoplay id='audio' preload='auto' tabindex='0' controls style='width:80%;max-width:400px;' type='audio/mpeg'>
                                    <source src='./audios/$namehref'>
                                    Sorry, your browser does not support HTML5 audio.
                                </audio>

	                            <ul id='playlist'>
     	                        ");
                            
                            
                            
                            
    	                
                            print("
      	  
                            <li class='active'>
 	                            <a href='./audios/$namehref'>$name</a>
 	                        </li>
	        
	                        ");
	                    
                            $flag=1;
	                    }
	
                        else{      
                            // Print 'em
                            print("
                            <li>
	                            <a href='./audios/$namehref'>$name</a>
	                        </li>
	                        ");
                        }
                    }
                }
      
            ?>
	
        </ul> 	

        </center>
    </body>
</html>
