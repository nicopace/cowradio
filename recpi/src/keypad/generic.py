import RPi.GPIO as GPIO
from time import sleep, time
import threading

class KeyPad():
    def __init__(self, rows, columns, **kwargs):
        """
	"""
        self.rows = rows
        self.columns = columns
        
        self.when_held = kwargs.pop('when_held', None)
        self.when_pressed = kwargs.pop('when_pressed', None)
        self.when_released = kwargs.pop('when_released', None)
        
        self.when_held_flag = False
        
        self._chk_inputs()
        
        self.timeout = kwargs.pop('timeout', None)
        
        self._initialize()
        
        #self.thread = threading.Thread(target=self.run, args=())
        #self.thread.daemon = True                            
        #self.thread.start()                                  
        
    #def __del__(self):
        #self.thread.join()
        
    def _chk_inputs(self):
        """ To check if value matrix is populated as per
            given rows and columns
        """
        if ((self.when_held is None) and (self.when_pressed is None) and
            (self.when_released is None) ):
                raise Exception("ValueError: Action Matrices entered incorrectly")
        
        x = len(self.rows)*len(self.columns)
        
        if (self.when_pressed is not None):
            y = len(self.when_pressed)
            
            for i in range(y):
                if x == (y*len(self.when_pressed[i])): continue
                else:
                    raise Exception("ValueError: when_pressed matrix entered incorrectly")
        
        if (self.when_released is not None):
            y = len(self.when_released)
            
            for i in range(y):
                if x == (y*len(self.when_released[i])): continue
                else:
                    raise Exception("ValueError: when_released matrix entered incorrectly")

            self.when_held_flag = []
            for i in range(y):
                temp = []
                for j in range(len(self.when_released[i])):
                    temp.append(False)

                self.when_held_flag.append(temp)


        if (self.when_held is not None):
            y = len(self.when_held)

            for i in range(y):
                if x == (y*len(self.when_held[i])): continue
                else:
                    raise Exception("ValueError: when_held matrix entered incorrectly")


        return

    def _initialize(self):
        """ To make all GPIO pins as input to disable keypad
        """
        GPIO.setmode(GPIO.BCM)
        for j in range(len(self.columns)):
            GPIO.setup(self.columns[j], GPIO.OUT)
            GPIO.output(self.columns[j], GPIO.HIGH)
    
        for i in range(len(self.rows)):
            GPIO.setup(self.rows[i], GPIO.IN, pull_up_down=GPIO.PUD_UP)
    
        
    def run(self):
        """ 
        """
        
        while True:
            
            for col_val in range(len(self.columns)):
                GPIO.output(self.columns[col_val], GPIO.LOW)
                
                for i in range(len(self.rows)):
                    temp = GPIO.input(self.rows[i])
                    if not temp:
                        if self.when_pressed is not None:
                            if (self.when_pressed[col_val][i] is not None):
                                self.when_pressed[col_val][i]()
                
                        temp = GPIO.input(self.rows[i])
                        t0 = time()
                        held = 0
                        while not temp:
                            t1 = time()
                            if (t1 - t0) > self.timeout:
                                held = 1
                                #while not temp:
                                #    temp = GPIO.input(self.rows[i])
                                break
                            temp = GPIO.input(self.rows[i])
                    
                        if held and (self.when_held[col_val][i] is not None):
                            self.when_held_flag[col_val][i] = True
                            self.when_held[col_val][i]()
                        elif self.when_released[col_val][i] is not None:
                            if not self.when_held_flag[col_val][i]:
                                self.when_released[col_val][i]()
                            else: self.when_held_flag[col_val][i] = False
                
                GPIO.output(self.columns[col_val], GPIO.HIGH)
            sleep(0.1)
