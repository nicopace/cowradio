from datetime import datetime
import time
import vlc

audiofile = "recorded@"+datetime.now().strftime('%d%b%Y_%H:%M')+".mp3"

def initialize_player(audiofile):
    "initialize  a vlc player which plays locally and saves to an mp3file"
    inst = vlc.Instance('-A alsa')   
    p = inst.media_player_new()   
    cmd0 = "alsa://plughw:1,0"
    cmd1 = "sout=#transcode{acodec=mp3,ab=320}:file{dst=%s}" %audiofile
    #cmd1 = "sout=#transcode{acodec=mp3}:#duplicate{dst=file{dst=%s},dst=display}" %audiofile
    cmd2 = "no-sout-rtp-sap"
    cmd3 = "no-sout-standard-sap"
    cmd4 = "sout-keep"
    med=inst.media_new(cmd0,cmd1,cmd2,cmd3,cmd4)   
    med.get_mrl()    
    p.set_media(med)
    return p, med

p,med = initialize_player(audiofile)
p.play()
time.sleep(20)
p.stop()
