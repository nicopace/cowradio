import subprocess

class OMXPlayer(object):
    def __init__(self, audio_dev='alsa:plughw:1,0'):
        ''' initializing omxplayer with the desired audio output device (sink) '''
        self.audio_dev = audio_dev
        self.process = ''

    def play(self, audio_file, start_time = '0'):
        ''' play the audio_file that is passed at the specified start_time '''
        self.process = subprocess.Popen(["omxplayer",
                                         "-o", self.audio_dev,
                                         "-l", start_time,
                                         audio_file],
                                        stdout=subprocess.PIPE,
                                        stdin=subprocess.PIPE,
                                        )
        if b'audioself.process.



