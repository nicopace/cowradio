#!/bin/bash

sudo systemctl stop hostapd
sudo service dhcpcd stop
sudo service dnsmasq stop
sudo cp /home/pi/cowradio/ap_mode/dnsmasq.conf /etc/
sudo cp /home/pi/cowradio/ap_mode/dhcpcd.conf /etc/
sudo service dhcpcd start
sudo service dnsmasq start
#sudo systemctl reload dnsmasq
#sudo systemctl unmask hostapd
#sudo systemctl enable hostapd
sudo systemctl start hostapd
